const express = require ('express')
const mongoose = require ('mongoose')
require('dotenv').config()
const app = express()
const port = 3001


//Mongoose connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@wdc028-course-booking.sevh8po.mongodb.net/b271-signup-db?retryWrites=true&w=majority`,{
	useNewUrlParser: true,
	useUnifiedTopology: true
})


let db = mongoose.connection
db.on("error", () => console.log("Connection Error"))
db.once("open", () => console.log("Connected to MongoDB!"))


//Middleware registration
app.use(express.json())
app.use(express.urlencoded({extended:true}))

// Signup Schemas
const signup_Schema = new mongoose.Schema({
	username: String,
	password: String
})

//Signup Model
const Signup = mongoose.model("Signup", signup_Schema)

//Signup Route
app.post('/signup', (request, response) => {
	Signup.findOne({username: request.body.username, password: request.body.password}).then((result, error) => {
		if(result.password == request.body.password  && result.username == request.body.username){
			return response.send("Existing user found!")

		}else { 
			let new_signup = new Signup({
				username: request.body.username,
				password: request.body.password
			})

			new_signup.save().then((created_signup, error) => {
				if(error){
					return console.log(error)
				}else {
					return response.status(201).send("New user registered!")
				}
			})
		}
	})
})

//Listen
app.listen(port, () => console.log(`The server is running at localhost: ${port}`))
